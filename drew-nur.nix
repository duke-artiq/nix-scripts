let
  pkgs = import <nixpkgs> { config.allowUnfree = true; };  # necessary for octet
  pulsecompilerSrc = <pulsecompilerSrc>;
  daxPulseSrc = <daxPulseSrc>;
  daxNix = <daxNix>;
  generatedNix = pkgs.runCommand "generated-nix" { buildInputs = [ pkgs.nix pkgs.git ]; }
    ''
      cp --no-preserve=mode,ownership -R ${./drew-nur} $out
      tar -xJf ${daxNix} -C $out

      REV=`git --git-dir ${pulsecompilerSrc}/.git rev-parse HEAD`
      cat > $out/pkgs/pulsecompiler-src.nix << EOF
      { fetchGit }:
      fetchGit {
        url = "ssh://git@gitlab.com/UMD-EURIQA/pulsecompiler.git";
        ref = "artiq-integration-octet-v1.2";
        rev = "$REV";
      }
      EOF

      REV=`git --git-dir ${daxPulseSrc}/.git rev-parse HEAD`
      cat > $out/pkgs/dax-pulse-src.nix << EOF
      { fetchGit }:
      fetchGit {
        url = "ssh://git@gitlab.com/duke-artiq/dax-pulse.git";
        ref = "feature/master-mts";
        rev = "$REV";
      }
      EOF
    '';
  nur-pkgs = import "${generatedNix}/default.nix" { inherit pkgs; };
  jobs = (builtins.mapAttrs (key: value: pkgs.lib.hydraJob value) nur-pkgs);
in
jobs // {
  drew-nur = pkgs.releaseTools.channel {
    name = "drew-nur";
    src = generatedNix;
    constituents = builtins.attrValues jobs;
  };
}
