{ pkgs ? import <nixpkgs> { config.allowUnfree = true; }
, daxpkgs ? import ./dax-full/default.nix { inherit pkgs; }
, # default to generated-nix structure for easy channel import w/ pinned dax/artiq version
  artiqpkgs ? import ./dax-full/fast/artiq-full/default.nix { inherit pkgs; }
}:

let
  pulsecompilerSrc = import ./pkgs/pulsecompiler-src.nix { inherit fetchGit; };

  pythonDeps = pkgs.callPackage ./pkgs/python-deps.nix { inherit pkgs pulsecompilerSrc; };

  pulsecompiler-full = pkgs.callPackage pulsecompilerSrc {
    inherit pkgs;
    pythonDeps = null;
    inherit (artiqpkgs) sipyco;
    inherit (pythonDeps) jaqalpaw octet drewrisinger-nur;
  };
  jobs = rec {
    inherit (pythonDeps) jaqalpaq jaqalpaw octet;
    inherit (pythonDeps.drewrisinger-nur.python3Packages) pygsti;
    pulsecompiler = pulsecompiler-full.pulsecompiler;
    pulsecompiler-docs = pulsecompiler-full.pulsecompiler-docs;
    dax-pulse = (pkgs.callPackage (import ./pkgs/dax-pulse-src.nix { inherit fetchGit; }) {
      inherit pkgs daxpkgs pulsecompiler;
    }).overridePythonAttrs (oldAttrs: { version = import ./pkgs/timestamp-version.nix { inherit (pkgs) stdenv; src = oldAttrs.src; }; });
  };
in
jobs
