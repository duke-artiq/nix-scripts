{ stdenv, src }:
let
  timestamp-version = stdenv.mkDerivation {
    name = "timestamp-version";
    inherit src;
    buildPhase = ''
      # create a version string based on the current time
      VERSION=`date --utc "+%Y%m%d%H%M"`
    '';
    installPhase = ''
      echo -n $VERSION > $out
    '';
  };
in
builtins.readFile timestamp-version
