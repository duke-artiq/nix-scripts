{ pkgs, pulsecompilerSrc }:

rec {
  drewrisinger-nur =
    let
      # filtering functions for NUR packages
      isReserved = n: n == "lib" || n == "overlays" || n == "modules";
      isDerivation = p: builtins.isAttrs p && p ? type && p.type == "derivation";
      isBuildable = p: !(p.meta.broken or false) && (p.meta.license.free or true) && !(p.meta.isRpiPkg or false);
      isCacheable = p: !(p.preferLocalBuild or false);
      shouldRecurseForDerivations = p: builtins.isAttrs p && p.recurseForDerivations or false;

      # recurse through derivations and filter
      buildJobset = s: (builtins.concatMap
        (n:
          if shouldRecurseForDerivations s.${n} then
            (buildJobset s.${n})
          else if !isReserved n && isDerivation s.${n} && isBuildable s.${n} && isCacheable s.${n} then
            [ (pkgs.lib.nameValuePair n (s.${n}.overrideAttrs (oldAttrs: rec { doCheck = false; }))) ]
          else [ ]
        )
        (builtins.attrNames s));

      src = fetchGit {
        url = "https://github.com/drewrisinger/nur-packages.git";
        ref = "master";
        rev = "c5ea02db33a4b41f53d53981c99be09ee8ce36b6";
      };

      drewrisinger-nur = import src { rawpkgs = pkgs; };
    in
    drewrisinger-nur;
  #(builtins.listToAttrs (buildJobset drewrisinger-nur));

  pulsecompiler-python-deps = import "${pulsecompilerSrc}/nix/python-dependencies.nix" {
    inherit pkgs drewrisinger-nur;
  };

  octet =
    let
      src = import (fetchGit {
        url = "ssh://git@gitlab.com/UMD-EURIQA/sandia-octet-mirror.git";
        ref = "master-mts/fix-packaging";
        rev = "7cd9b0ec3b298dd613f5996b1bc9fa7d95c06a92";
      });
    in
    pkgs.callPackage src { inherit pkgs; };

  jaqalpaq =
    let
      src = builtins.fetchGit {
        url = "git@gitlab.com:UMD-EURIQA/jaqalpaq-mirror.git";
        ref = "master-mts";
        rev = "407da7d74de9a07aa8791f64e9f81fbad8afb414";
      };
    in
    (pulsecompiler-python-deps.jaqalpaq.overrideAttrs (oa: {
      src = src;
    }));

  jaqalpaw =
    let
      src = builtins.fetchGit {
        url = "git@gitlab.com:UMD-EURIQA/jaqalpaw-mirror.git";
        ref = "1-no-rounding-in-convert_amp_full";
        rev = "c5b468b939029d09e048900645ec1c22d81a04ea";
      };
    in
    (pulsecompiler-python-deps.jaqalpaw.overrideAttrs (oa: {
      src = src;
    }));
}
