{
  inputs = {
    artiqpkgs.url = git+https://github.com/m-labs/artiq?ref=release-7;
    nixpkgs.follows = "artiqpkgs/nixpkgs";
    sipyco.follows = "artiqpkgs/sipyco";
    artiq-extrapkg = {
      url = git+https://git.m-labs.hk/m-labs/artiq-extrapkg?ref=release-7;
      inputs = {
        artiq.follows = "artiqpkgs";
      };
    };

    # public
    dax = {
      url = git+https://gitlab.com/duke-artiq/dax.git;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
        flake8-artiq.follows = "flake8-artiq";
        artiq-stubs.follows = "artiq-stubs";
        trap-dac-utils.follows = "trap-dac-utils";
      };
    };
    dax-comtools = {
      url = git+https://gitlab.com/duke-artiq/dax-comtools.git;
      inputs = {
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
      };
    };
    dax-applets = {
      url = git+https://gitlab.com/duke-artiq/dax-applets.git;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
      };
    };
    flake8-artiq = {
      url = git+https://gitlab.com/duke-artiq/flake8-artiq.git;
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    artiq-stubs = {
      url = git+https://gitlab.com/duke-artiq/artiq-stubs.git?ref=release-7;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
        flake8-artiq.follows = "flake8-artiq";
      };
    };
    h5-artiq = {
      url = git+https://gitlab.com/duke-artiq/h5-artiq.git;
      inputs = {
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
      };
    };
    trap-dac-utils = {
      url = git+https://gitlab.com/duke-artiq/trap-dac-utils.git;
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };

    # private
    # todo: if people run into issues using this flake on hosts without gitlab ssh access set up,
    #  could try removing private repos from inputs and using e.g. fetchgit
    dax-comtools-private = {
      url = git+ssh://git@gitlab.com/duke-artiq/dax-comtools-private;
      inputs = {
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
        dax-comtools.follows = "dax-comtools";
      };
    };
    dax-gateware = {
      url = git+ssh://git@gitlab.com/duke-artiq/dax-gateware;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
      };
    };
    pulselib = {
      url = git+ssh://git@gitlab.com/duke-artiq/pulselib;
      inputs = {
        nixpkgs.follows = "nixpkgs";
      };
    };
    dax-program-sim = {
      url = git+ssh://git@gitlab.com/duke-artiq/dax-program-sim;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
        daxpkgs.follows = "dax";
      };
    };
    dax-pulse = {
      url = git+ssh://git@gitlab.com/duke-artiq/dax-pulse;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
        sipyco.follows = "sipyco";
        daxpkgs.follows = "dax";
      };
    };

    # extra inputs
    drew-nur = {
      type = "github";
      owner = "drewrisinger";
      repo = "nur-packages";
      rev = "295ca29567e8e4fc2b4329d24bc1739d46fb7768";
      narHash = "sha256-i0bF1vkeI/I5TAGYNMrPrE8n3SMVcY5JfUiAqqZ4oYo=";
      flake = false;
    };
    gmqtt-src = {
      type = "github";
      owner = "wialon";
      repo = "gmqtt";
      ref = "v0.6.11";
      rev = "8f449b016e441075c8507b653ccafaa8b1615aeb";
      flake = false;
    };
    miniconf-mqtt-src = {
      type = "github";
      owner = "quartiq";
      repo = "miniconf";
      rev = "8b09f2afcb3a6fb7c64cec778c35c3f0b1badf56";
      narHash = "sha256-3CLJmgc2ai9i/vIrxdVHhzNEI71CLdldP+lecwUQ/to=";
      flake = false;
    };
    stabilizer-src = {
      type = "github";
      owner = "quartiq";
      repo = "stabilizer";
      rev = "4dd3ecf802888430ca9306f2c52f9a3574810ec3";
      narHash = "sha256-4GZ2hGH8FVnPvpy3rCJUozxfn9R98winhPlDDc3C1FE=";
      flake = false;
    };
    arb-interp-src = {
      type = "tarball";
      url = "https://github.com/DurhamDecLab/ARBInterp/releases/download/v1.6/ARBInterp-1.6.tar.gz";
      narHash = "sha256-47jVFGRJQtsKOmjU4jixpvJlus7GeUvNL5e9J8QqLZA=";
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , artiqpkgs
    , artiq-extrapkg
    , dax
    , dax-comtools
    , dax-applets
    , flake8-artiq
    , artiq-stubs
    , dax-comtools-private
    , dax-gateware
    , h5-artiq
    , trap-dac-utils
    , pulselib
    , dax-program-sim
    , dax-pulse
    , ...
    }@extraInputs:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      nixPackage = inputFlake: pkgName: (
        inputFlake.packages.x86_64-linux.${pkgName}
      );

      mainPackages = {
        dax = nixPackage dax "dax";
        dax-comtools = nixPackage dax-comtools "dax-comtools";
        dax-applets = nixPackage dax-applets "dax-applets";
        flake8-artiq = nixPackage flake8-artiq "flake8-artiq";
        artiq-stubs = nixPackage artiq-stubs "artiq-stubs";
        h5-artiq = nixPackage h5-artiq "h5-artiq";
        trap-dac-utils = nixPackage trap-dac-utils "trap-dac-utils";

        dax-comtools-private = nixPackage dax-comtools-private "dax-comtools-private";
        dax-gateware = nixPackage dax-gateware "dax-gateware";
        pulselib = nixPackage pulselib "pulselib";
        dax-program-sim = nixPackage dax-program-sim "dax-program-sim";
        dax-pulse = nixPackage dax-pulse "dax-pulse";
      };

      nur-packages =
        let
          # filtering functions for NUR packages
          isReserved = n: n == "lib" || n == "overlays" || n == "modules";
          isDerivation = p: builtins.isAttrs p && p ? type && p.type == "derivation";
          isBuildable = p: !(p.meta.broken or false) && (p.meta.license.free or true) && !(p.meta.isRpiPkg or false);
          isCacheable = p: !(p.preferLocalBuild or false);
          shouldRecurseForDerivations = p: builtins.isAttrs p && p.recurseForDerivations or false;

          # recurse through derivations and filter
          buildJobset = s: (builtins.concatMap
            (n:
              if shouldRecurseForDerivations s.${n} then
                (buildJobset s.${n})
              else if !isReserved n && isDerivation s.${n} && isBuildable s.${n} && isCacheable s.${n} then
                [ (pkgs.lib.nameValuePair n (s.${n}.overrideAttrs (oldAttrs: rec { doCheck = false; }))) ]
              else [ ]
            )
            (builtins.attrNames s));

          overlays = import "${extraInputs.drew-nur}/overlays";
          pkg-overlays = (if builtins.isAttrs overlays then builtins.attrValues overlays else overlays);
          drew-nur = import extraInputs.drew-nur {
            inherit overlays;
            pkgs = import nixpkgs { system = "x86_64-linux"; overlays = pkg-overlays; };
          };
        in
        (builtins.listToAttrs (buildJobset drew-nur));

      extraPackages = rec {
        inherit (dax-comtools-private.packages.x86_64-linux) instrumentkit labjack-ljm simple-pid toptica-lasersdk;
        inherit (dax-gateware.packages.x86_64-linux) entangler-core;
        inherit (dax-comtools.packages.x86_64-linux) pymoku;
        inherit (nur-packages) pygsti dynaconf;
        gmqtt = pkgs.python3Packages.buildPythonPackage rec {
          pname = "gmqtt";
          version = "0.6.11";

          src = extraInputs.gmqtt-src;

          checkInputs = with pkgs.python3Packages; [ pytestCheckHook ];
        };

        miniconf-mqtt = pkgs.python3Packages.buildPythonPackage rec {
          pname = "miniconf-mqtt";
          version = "0.5.0+g${extraInputs.miniconf-mqtt-src.shortRev}";
          doCheck = false; # no tests

          src = extraInputs.miniconf-mqtt-src + "/py/${pname}";

          propagatedBuildInputs = [ gmqtt ];
          condaDependencies = [ "gmqtt" ];
        };

        stabilizer = pkgs.python3Packages.buildPythonPackage rec {
          pname = "stabilizer";
          version = "0.6.0+g${extraInputs.stabilizer-src.shortRev}";
          doCheck = false; # no tests

          src = extraInputs.stabilizer-src + "/py";

          patches = [ ./patches/stabilizer.patch ];

          propagatedBuildInputs = (with pkgs.python3Packages; [ numpy ]) ++ [ miniconf-mqtt ];
          condaDependencies = [ "numpy" "miniconf-mqtt" ];
        };

        arb-interp = pkgs.python3Packages.buildPythonPackage rec {
          pname = "ARBInterp";
          version = "1.6";

          src = extraInputs.arb-interp-src;

          propagatedBuildInputs = with pkgs.python3Packages; [ numpy ];
        };
      };

      condaPackages = builtins.map
        (
          value: {
            name = value.pname;
            version = value.version;
            src = value.src;
            dependencies = value.condaDependencies;
          }
        )
        (builtins.filter (el: el ? condaDependencies) (builtins.attrValues (mainPackages // extraPackages)));

      conda-channel = artiq-extrapkg.makeCondaChannel condaPackages;

      nixJobs = mainPackages // extraPackages;
      allJobs = nixJobs // { inherit conda-channel; };

      # Due to Hydra's lack of (useful) support for flakes, we periodically update & publish the lock file using gitlab CI.
      # In order to keep things in sync between the flake and the binary cache, re-publish the flake & lock file as a "channel."
      # The `channel` derivation is useful because the build only succeeds if all other builds in the jobset succeed and it publishes a nice, easy-to-use tarball.
      dax-fast = pkgs.releaseTools.channel {
        name = "dax-fast";
        src = ./.;
        constituents = builtins.attrValues allJobs;
        isNixOS = false;
      };
    in
    rec {
      packages.x86_64-linux = nixJobs;
      hydraJobs = allJobs // { inherit dax-fast; };
      # enables use of `nix fmt`
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;
    };

  nixConfig = {
    extra-trusted-public-keys = "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc=";
    extra-substituters = "https://nixbld.m-labs.hk";
    extra-sandbox-paths = "/opt";
    allow-import-from-derivation = true;
    allowed-uris = [
      "https://github.com/m-labs/"
      "https://gitlab.com/duke-artiq"
      "ssh://git@gitlab.com/duke-artiq"
      "https://files.pythonhosted.org"
      "https://git.m-labs.hk/m-labs/"
      "https://github.com/DurhamDecLab/ARBInterp"
    ];
  };
}
