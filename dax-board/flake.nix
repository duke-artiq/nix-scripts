{
  inputs = {
    artiqpkgs.url = git+https://github.com/m-labs/artiq?ref=release-7;
    nixpkgs.follows = "artiqpkgs/nixpkgs";
    artiq-zynq.url = git+https://git.m-labs.hk/m-labs/artiq-zynq?ref=release-7;

    dax-gateware = {
      url = git+ssh://git@gitlab.com/duke-artiq/dax-gateware;
      inputs = {
        artiqpkgs.follows = "artiqpkgs";
        nixpkgs.follows = "nixpkgs";
      };
    };

    dax-gateware-json = {
      url = git+https://gitlab.com/duke-artiq/dax-gateware-json;
      flake = false;
    };
  };

  outputs =
    { self
    , nixpkgs
    , artiqpkgs
    , artiq-zynq
    , dax-gateware
    , dax-gateware-json
    }:
    let
      pkgs = import nixpkgs { system = "x86_64-linux"; };
      # want to take artiqpkgs.makeArtiqBoardPackage.nativeBuildInputs and replace their 
      # python environment with our own, containing dax-gateware
      # also need to replace vivado to match our version
      filterfunc = drv: !((pkgs.lib.strings.hasPrefix "python3" drv.name) || drv.name == "vivado");
      vivado = artiqpkgs.packages.x86_64-linux.vivado.overrideAttrs (oldAttrs: {
        profile = "set -e; source /opt/Xilinx/Vivado/2022.1/settings64.sh";
      });
      dax-board-builder = { target, variant, buildCommand ? "python -m dax_gateware.targets.${target} ${variant}" }:
        (artiqpkgs.makeArtiqBoardPackage { inherit target variant buildCommand; }).overrideAttrs (oldAttrs: rec {
          nativeBuildInputs = (
            builtins.filter filterfunc oldAttrs.nativeBuildInputs
          ) ++ [
            (pkgs.python3.withPackages (ps:
              (with artiqpkgs.packages.x86_64-linux; [ artiq migen misoc ]) ++
                [ dax-gateware.packages.x86_64-linux.dax-gateware ]
            ))
            vivado
          ];
        });

      variants = [
        # "brassboard"
        "gold_master"
        "gold_satellite"
        # "cold_qalcium"
        # "don"
        # "ionphoton"
        # "molecular_ions"
        # "motion"
        # "staq"
        # "staq_sat"
        "rayquaza"
        "green_master"
        "green_satellite"
        "compact_cryo"
        "compact_cryo_master"
        "compact_cryo_satellite_dac"
        "strontium"
        "strontium_suservo"
        "strontium_master"
        "strontium_suservo_satellite"
        "qsim"
        "tester_10"
        "tester_11"
        "tester_20"
        "artiq_demo"
      ];
      kasliPackages = pkgs.lib.lists.foldr
        (variant: start:
          let
            json = builtins.toPath "${dax-gateware-json}/${variant}.json";
          in
          start // {
            "artiq-board-kasli-${variant}" = dax-board-builder {
              target = "kasli";
              variant = variant;
              buildCommand = "python -m dax_gateware.targets.kasli ${json}";
            };
          } // (pkgs.lib.optionalAttrs ((builtins.fromJSON (builtins.readFile json)).base == "standalone") {
            "device-db-kasli-${variant}" = pkgs.stdenv.mkDerivation {
              name = "device-db-kasli-${variant}";
              buildInputs = [ dax-gateware.packages.x86_64-linux.dax-gateware ];
              phases = [ "buildPhase" ];
              buildPhase = "
                mkdir $out
                python -m dax_gateware.ddb_template ${json} -o $out/device_db.py
                mkdir $out/nix-support
                echo file device_db_template $out/device_db.py >> $out/nix-support/hydra-build-products
              ";
            };
          }))
        { }
        variants;

      soc-variants = [
        "tester_kasli_soc"
      ];

      kasliSOCPackages = pkgs.lib.lists.foldr
        (variant: start:
          let
            json = builtins.toPath "${dax-gateware-json}/${variant}.json";
          in
          start // (
            artiq-zynq.makeArtiqZynqPackage {
              target = "kasli_soc";
              variant = variant;
              json = json;
            }
          ) // (pkgs.lib.optionalAttrs ((builtins.fromJSON (builtins.readFile json)).base == "standalone") {
            "device-db-kasli-${variant}" = pkgs.stdenv.mkDerivation {
              name = "device-db-kasli-${variant}";
              buildInputs = [ dax-gateware.packages.x86_64-linux.dax-gateware ];
              phases = [ "buildPhase" ];
              buildPhase = "
                mkdir $out
                python -m dax_gateware.ddb_template ${json} -o $out/device_db.py
                mkdir $out/nix-support
                echo file device_db_template $out/device_db.py >> $out/nix-support/hydra-build-products
              ";
            };
          }))
        { }
        soc-variants;

      jobs = kasliPackages // kasliSOCPackages;
      dax-board = pkgs.releaseTools.channel {
        name = "dax-board";
        src = ./.;
        constituents = builtins.attrValues jobs;
        isNixOS = false;
      };
    in
    rec {
      packages.x86_64-linux = jobs;
      hydraJobs = jobs // { inherit dax-board; };
      # enables use of `nix fmt`
      formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;
      # facilitate local building using dax-gateware
      inherit dax-board-builder;
    };

  nixConfig = {
    extra-trusted-public-keys = "nixbld.m-labs.hk-1:5aSRVA5b320xbNvu30tqxVPXpld73bhtOeH6uAjRyHc=";
    extra-substituters = "https://nixbld.m-labs.hk";
    extra-sandbox-paths = "/opt";
    allow-import-from-derivation = true;
    allowed-uris = [
      "https://github.com/m-labs/"
      "https://gitlab.com/duke-artiq"
      "ssh://git@gitlab.com/duke-artiq"
      "https://files.pythonhosted.org"
      "https://git.m-labs.hk/m-labs/"
    ];
  };
}
